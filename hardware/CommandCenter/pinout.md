Pinout

Microcontroller
  
  * PA0 DalekBus TX
  * PA1 DalekBus RX
  * PA4 Intruder detection

  * PB6 SCL 1
  * PB7 SDA 1
  
  * PC0 Hangar Stepper enable
  * PC1 Hangar Stepper direction
  * PC2 Hangar Stepper Step
  * PC3 Hangin EndStop

PCA 9685 1
  * PWM 00 Left Stearing Joystick
  * PWM 01 Right Stearing Joystick
  * PWM 02 Hangar Hatch
  * PWM 03 Robot Arm Ground
  * PWM 04 Robot Arm Shoulder
  * PWM 05 Robot Arm Hand
  * PWM 06 Disco Ball
  * PWM 07 Dalek Dance 
  * PWM 08 First Gauge
  * PWM 09 Second Gauge
  * PWM 10 Third Gauge
  * PWM 11 Fist Gauge Light
  * PWM 12 Second Gauge Light
  * PWM 13 Third Gauge Light
  * PWM 14 Air Pump Fogger
  * PWM 15 Drone Hangar Light

PCA 9685 2
  * PWM 00 Seat Light R
  * PWM 01 Seat Light G
  * PWM 02 Seat Light B
  * PWM 03 Main Light R
  * PWM 04 Main Light G
  * PWM 05 Main Light B
  * PWM 06 Disco Ball R
  * PWM 07 Disco Ball G
  * PWM 08 Disco Ball B
  * PWM 09 Fan
  * PWM 10 Fogger
  * PWM 11 Laser Diode 1 
  * PWM 12 Laser Diode 2
  * PWM 13 Laser Diode 3
  * PWM 14 Laser Diode 4
  * PWM 15 Laser Diode 5
  
PCA 9685 2
  * PWM 00 Computer 1 Left
  * PWM 01 Computer 1 Right
  * PWM 02 Computer 2 Left
  * PWM 03 Computer 2 Right
  * PWM 04 Computer 3 Left
  * PWM 05 Computer 3 Right
  * PWM 06 Washing Machine 
  * PWM 07 Disco USB Left
  * PWM 08 Disco USB Right
  * PWM 09 Servo White Flag 
  * PWM 10 Consciousness Eye
  * PWM 11 
  * PWM 12 
  * PWM 13 
  * PWM 14
  * PWM 15 

  