#include "dalek_i2c.h"
#include "main.h"
#include "cmsis_os.h"
#include "queue.h"
#include "dalek_i2c.h"
#include "stdbool.h"


extern I2C_HandleTypeDef hi2c1;

uint8_t I2C_DATA = 0x00;


void DalekI2cTask(void *argument){

  while (true){
    DalekI2cQueueMessage_t m;

    if(xQueueReceive(dalekI2cQueueHandle, &m, 0) == pdTRUE){
      // do magic here
    }

    osDelay(1);
  }
}
