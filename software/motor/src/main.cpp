#include <stdint.h>
#include <SoftwareSerial.h>
#include <aREST.h>
#include "pinout.h"
#include "motor.h"

SoftwareSerial Serial1S(PIN_BLUETOOTH_RX, PIN_BLUETOOTH_TX);

//SoftwareSerial StoPI(5,3);



aREST rest = aREST();

int motorControl(String command) {

  command.replace("/","");
  Serial.println(command);

  if (command.indexOf("=") != -1){
    int id = 0;
    int value = 0;
    char delimiter[] = "=";
    char stringBuffer[command.length() + 1];
    command.toCharArray(stringBuffer, command.length() + 1);
    char *ptr = strtok(stringBuffer, delimiter);

    while (ptr != NULL){

      if (0 != id){
        value = atoi(ptr);
      }else{
        id = atoi(ptr);
      }
      ptr = strtok(NULL, delimiter);
    }
    if (id != 0 && value != 0)
    motor_speed(id,value);
    return 1;
  }else{
    return 0;
  }
}

int lightControl(String command) {
  command.replace("/","");
  command.replace("=","");
  // Serial.print("Licht : " );
  // Serial.println(command);

    int value = 0;
    char delimiter[] = "=";
    char stringBuffer[command.length() + 1];
    command.toCharArray(stringBuffer, command.length() + 1);
    char *ptr = strtok(stringBuffer, delimiter);

    value = atoi(ptr);

    digitalWrite(5, LOW);
    digitalWrite(6, LOW);
    digitalWrite(7, LOW);

    if (value >= 4) {
      digitalWrite(5, HIGH);
      value = value -4;
    }
    if (value >= 2) {
      digitalWrite(6, HIGH);
      value = value -2;
    }
    if (value >= 1) {
      digitalWrite(7, HIGH);
      value = value -1;
    }
    return 1;
  
}

int toPi (String command) {
  //StoPI.println(command);
}

void setup() {
  Serial1S.begin(19200);
 // StoPI.begin(19200);
  Serial.begin(9600);
  motor_setup();
  //rest.function((char*)"motorControl", motorControl);
  // rest.function((char*)"toPi", toPi);
  Serial.println("Init DONE");
  Serial1S.setTimeout(10);
}

void loop() {
  //rest.handle(Serial1S);
  while (Serial1S.available()) {
    String command = Serial1S.readString();
    
    //Serial.println(command);


    // if (command.startsWith("Light")) {
    //     command.replace("Light","");
    //     lightControl(command);
    // } else if (command.startsWith("motor=")) {
    //   command.replace("motor=","");
      motorControl(command);
    // }
  }
  //for (uint8_t i = 0; i < 10; i++){
    motor_control();
    // delay(3);
  //}

}

