//
//  AppDelegate.h
//  dalekremote
//
//  Created by Daniel Kalwitzki on 11.03.19.
//  Copyright © 2019 Daniel Kalwitzki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

