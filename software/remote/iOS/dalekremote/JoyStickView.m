//
//  VisualStickView.m
//  SampleGame
//
//  Created by Zhang Xiang on 13-4-26.
//  Copyright (c) 2013年 Myst. All rights reserved.
//

#import "JoyStickView.h"

#define STICK_CENTER_TARGET_POS_LEN 20.0f

@implementation JoyStickView

-(void) initStick
{
    imgStickNormal = [UIImage imageNamed:@"stick_normal.png"];
    imgStickHold = [UIImage imageNamed:@"stick_hold.png"];
//    stickView.image = imgStickNormal;
    mCenter.x = 190;
    mCenter.y = 190;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initStick];
    }
    return self;
}


- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
	{
        // Initialization code
        [self initStick];
    }
	
    return self;
}

- (void)dealloc {

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)notifyDir:(CGPoint)dir
{
  
    
    NSValue *vdir = [NSValue valueWithCGPoint:dir];
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                              vdir, @"dir", nil];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"StickChanged" object:nil userInfo:userInfo];
    
}

- (void)stickMoveTo:(CGPoint)deltaToCenter
{
    CGRect fr = stickView.frame;
    fr.origin.x = deltaToCenter.x;
    fr.origin.y = deltaToCenter.y;
    stickView.frame = fr;
}

- (void)touchEvent:(NSSet *)touches
{
    int scale = 180;
    if([touches count] != 1)
        return ;
    
    UITouch *touch = [touches anyObject];
    UIView *view = [touch view];
    if(view != self)
        return ;
    int dim = self.frame.size.height;
    CGPoint touchPoint = [touch locationInView:self];
    CGPoint dtarget, dir, absolut;
    dir.x = touchPoint.x - mCenter.x;
    dir.y = touchPoint.y - mCenter.y;
    
    //*(float)-1;// / (float)view.frame.size.width ;
    float valy = ((((float)touchPoint.y)/(float)view.frame.size.height)*scale);//((((int)touchPoint.y)/view.frame.size.height)*scale-scale)*-1;
    //NSLog(@"%f",valy);
    if (valy > scale)
        valy = scale;
    if (valy < 0)
        valy = 0;
    
    int valx = ((((float)touchPoint.x)/(float)view.frame.size.width)*scale*-1)+scale;
    
    if (valx > scale)
        valx = scale;
    if (valx < 0)
        valx = 0;
    
    float steeringfactor = 0.5;
    
    int left = valy-((valx-90)*steeringfactor);
    int right = valy+((valx-90)*steeringfactor);
    if (left > scale)
        left = scale;
    if (left < 0)
        left = 0;
    if (right > scale)
        right = scale;
    if (right < 0)
        right = 0;
    
    
    absolut.y = left;
    absolut.x = right;
    double len = sqrt(dir.x * dir.x + dir.y * dir.y);

    if(len < 10.0 && len > -10.0)
    {
        // center pos
        dtarget.x = 0.0;
        dtarget.y = 0.0;
        dir.x = 0;
        dir.y = 0;
    }
    else
    {
        double len_inv = (1.0 / len);
        dir.x *= len_inv;
        dir.y *= len_inv;
        dtarget.x = dir.x * STICK_CENTER_TARGET_POS_LEN;
        dtarget.y = dir.y * STICK_CENTER_TARGET_POS_LEN;
       
    }
    [self stickMoveTo:dtarget];
    
    [self notifyDir:absolut];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    stickView.image = imgStickHold;
    [self touchEvent:touches];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchEvent:touches];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    stickView.image = imgStickNormal;
    CGPoint dtarget, dir, absolut;
    dir.x = dtarget.x = 0.0;
    dir.y = dtarget.y = 0.0;
    [self stickMoveTo:dtarget];
    absolut.x = 90;
    absolut.y = 90;
    [self notifyDir:absolut];
}

@end
