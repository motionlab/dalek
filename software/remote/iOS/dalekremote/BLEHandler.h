//
//  BLEHandler.h
//  odleTV
//
//  Created by Daniel Kalwitzki on 21.04.17.
//  Copyright © 2017 Daniel Kalwitzki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "AppDelegate.h"

@interface BLEHandler : NSObject <CBPeripheralDelegate, CBCentralManagerDelegate, CBPeripheralManagerDelegate, CBPeripheralManagerDelegate>

+ (BLEHandler*)sharedInstance;
-(void) disconnectAll;
-(void) writeValue:(NSString*)svalue;

@property(strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) CBCentralManager *centralManager;
@property (nonatomic, strong) NSTimer *connectTimer;
@property (nonatomic, assign) int connectTime;
@property (nonatomic, strong) id delegate;

@property (strong, nonatomic) NSMutableDictionary *devices;
@property (nonatomic, strong) NSMutableArray *connectQueue;
@property (strong, nonatomic) NSMutableArray *discoveredDevices;
@property (strong, nonatomic) CBPeripheral *discoveredPeripheral;
@property (strong, nonatomic) CBPeripheral *selectedPeripheral;
@property (readonly, nonatomic) CFUUIDRef UUID;
@property (strong, nonatomic) CBCharacteristic *characteristics;
@property (strong, nonatomic) NSMutableData *data;

@property (strong, nonatomic) NSMutableDictionary *deviceData;
@property (strong, nonatomic) NSMutableDictionary *deviceDataDict;

@end

@protocol BLEHandlerDelegate <NSObject>

@optional
-(void) setInstance:(NSString*)instance;


@end


