# dalek

## Safety system pinout
The current pin connections for the echo signal of each sensor.  
Starting from the center back, clockwise.

|Sensor |Pin |
|-------|----|
|1      |D5  |
|2      |D6  |
|3      |D7  |
|4      |D8  |
|5      |D10 |
|6      |D9  |
|7      |D11 |
|8      |D3  |
|9      |D12 |
|10     |D13 |
|11     |D4  |