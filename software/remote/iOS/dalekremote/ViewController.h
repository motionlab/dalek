//
//  ViewController.h
//  dalekremote
//
//  Created by Daniel Kalwitzki on 11.03.19.
//  Copyright © 2019 Daniel Kalwitzki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>

@interface ViewController : UIViewController <AVAudioPlayerDelegate>
@property (nonatomic, strong) NSString *sendval;
@property (nonatomic, strong) AVAudioPlayer *player;
@end

