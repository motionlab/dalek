#include "main.h"
#include "stdbool.h"
#include "cmsis_os.h"
#include "queue.h"
#include "math.h"
#include "dalek_i2c.h"
#include "dalek_orchestration.h"
#include "dalek_orchestra.h"

void DalekConsciousnessTask(void *argument){

  while(true){
    // Do crazy stuff to get numbers of pi
    osDelay(2000);
    DalekOrchestraEyeBlink(3);
    osDelay(10000);
    DalekOrchestraEyeBlink(1);
    osDelay(10000);
    DalekOrchestraEyeBlink(4);
    osDelay(10000);
    DalekOrchestraEyeBlink(1);
    osDelay(10000);
    DalekOrchestraEyeBlink(5);
    osDelay(10000);
    DalekOrchestraEyeBlink(9);
    osDelay(10000);
    DalekOrchestraEyeBlink(2);
    osDelay(10000);
    DalekOrchestraEyeBlink(6);
    osDelay(10000);
    DalekOrchestraEyeBlink(5);
    osDelay(10000);
    DalekOrchestraEyeBlink(3);
    osDelay(10000);
    DalekOrchestraEyeBlink(9);
    osDelay(23000);
  }

}
