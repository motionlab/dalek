//
//  BLEHandler.m
//  odleTV
//
//  Created by Daniel Kalwitzki on 21.04.17.
//  Copyright © 2017 Daniel Kalwitzki. All rights reserved.
//

#import "BLEHandler.h"

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad
#define IPHONE   UIUserInterfaceIdiomPhone
#define ATV      UIUserInterfaceIdiomTV


@implementation BLEHandler

+ (BLEHandler*)sharedInstance {
    static BLEHandler *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        if (nil == _connectQueue) {
            _connectQueue = [[NSMutableArray alloc] init];
        }
        if (0 == _connectTime) {
            if ([self isATV]) {
                _connectTime = 30;
            } else {
                _connectTime = 20;
            }
        }
        _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _connectTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(connectTimerFire:) userInfo:nil repeats:YES];
        [_connectTimer fire];
    }
    return self;
}

-(BOOL) isIphone {
    if ( IDIOM == IPHONE ) {
        return YES;
    } else {
        return NO;
    }
}

-(BOOL) isIpad {
    if ( IDIOM == IPAD ) {
        return YES;
    } else {
        return NO;
    }
}

-(BOOL) isATV {
    if ( IDIOM == ATV ) {
        return YES;
    } else {
        return NO;
    }
}

-(void) connectTimerFire:(id)object {
    
    if (nil == _centralManager)
        _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
   
    if (nil != _connectQueue && _connectQueue.count > 0) {
        
        CBPeripheral *connectDevice = [_connectQueue objectAtIndex:0];
        
        // move Object to the last position
        [_connectQueue removeObject:connectDevice];
        [_connectQueue addObject:connectDevice];
        
        // connect to the old first entry
        [_centralManager connectPeripheral:connectDevice options:nil];
    }
 [_connectTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:_connectTime]];
    NSLog(@"Connect fire on %@",[NSDate date]);
}


#pragma mark Bluetooth

-(void) disconnectAll {
    for (CBPeripheral *per in _connectQueue) {
        [_centralManager cancelPeripheralConnection:per];
    }
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    if (central.state != CBManagerStatePoweredOn) {
        return;
    }
    if (central.state == CBManagerStatePoweredOn) {
        
        [_centralManager scanForPeripheralsWithServices:nil options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
    }
}

-(void) writeValue:(NSString*)svalue {
    [_selectedPeripheral writeValue:[svalue dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:_characteristics type:CBCharacteristicWriteWithoutResponse];
}
- (NSMutableDictionary *)devices
{
    if (_devices == nil)
    {
        _devices = [NSMutableDictionary dictionaryWithCapacity:6];
    }
    
    return _devices;
}

- (void)centralManager:(CBCentralManager *)central
 didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary *)advertisementData
                  RSSI:(NSNumber *)RSSI
{
    _discoveredPeripheral = peripheral;
    
    NSString * uuid = [[peripheral identifier] UUIDString];
    
    if (uuid && [peripheral.name hasPrefix:@"DALEK"])
    {
        [self.devices setObject:peripheral forKey:uuid];
        if (nil == _discoveredDevices) {
            _discoveredDevices = [[NSMutableArray alloc] init];
        }
        
        if (NSNotFound == [_connectQueue indexOfObject:peripheral]) {
            // Device not in connect Queue
            [_connectQueue addObject:peripheral];
        }
        
    }
}

-(void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"Did fail to connect to %@",peripheral.name);
//    NSLog(@"Connect Queue count %ld",_connectQueue.count);
}

-(void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"Disconnected from %@",peripheral.name);
    NSLog(@"Connect Queue count %ld",_connectQueue.count);
    [self checkForConnection:nil];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
   NSLog(@"Created Connection to %@",peripheral.name);
    peripheral.delegate = self;
//    NSLog(@"Connect Queue count %ld",_connectQueue.count);
    [peripheral discoverServices:nil];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    for (CBService * service in [peripheral services])
    {
        [peripheral discoverCharacteristics:nil forService:service];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral
didDiscoverCharacteristicsForService:(CBService *)service
             error:(NSError *)error
{
    for (CBCharacteristic * character in [service characteristics])
    {
        [peripheral discoverDescriptorsForCharacteristic:character];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral
didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic
             error:(NSError *)error
{
    // ADA 6E400003-B5A3-F393-E0A9-E50E24DCCA9E
    // HM FFE1
    
    NSString *name = [[characteristic UUID] UUIDString];
    if ([name isEqualToString:@"FFE1"] || [name isEqualToString:@"6E400003-B5A3-F393-E0A9-E50E24DCCA9E"]) {
        _selectedPeripheral = peripheral;
        for (CBService * service in [peripheral services])
        {
            _characteristics = characteristic;
            
            for (CBCharacteristic * characteristic in [service characteristics])
            {
                [peripheral setNotifyValue:true forCharacteristic:characteristic];
                [NSTimer scheduledTimerWithTimeInterval:10 repeats:NO block:^(NSTimer * _Nonnull timer) {
            //        [_centralManager cancelPeripheralConnection:peripheral];
                }];
            }
        }
    } else if ([name isEqualToString:@"6E400003-B5A3-F393-E0A9-E50E24DCCA9E"]) {
        _characteristics = characteristic;
    }
}


-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    
    NSString * str = [[NSString alloc] initWithData:[characteristic value] encoding:NSASCIIStringEncoding];
    //    if (debug)
    if (nil == str)
        str = [[NSString alloc] initWithData:[characteristic value] encoding:NSASCIIStringEncoding];
    
    
    
}


-(void) checkForConnection:(id)sender {
    NSMutableArray *UUIDS = [[NSMutableArray alloc] init];
}


- (void)peripheralManagerDidUpdateState:(nonnull CBPeripheralManager *)peripheral {
    
}

@end
