#include "BLEDevice.h"
 
#define HM_MAC "a4:d5:78:69:c0:f1"  // Bitte Adresse des HM-10 Moduls hier eintragen
uint32_t PIN =  ;             // pairing password PIN Passwort PIN-Code Kennwort
 
// Service und Characteristic des HM-10 Moduls
static BLEUUID serviceUUID("0000FFE0-0000-1000-8000-00805F9B34FB");
static BLEUUID charUUID("0000FFE1-0000-1000-8000-00805F9B34FB");
 
static BLEAddress *pServerAddress;
static boolean Verbinde = true;
static boolean Verbunden = false;
static BLERemoteCharacteristic* pRemoteCharacteristic;
BLEClient*  pClient;
 
class MySecurity : public BLESecurityCallbacks
{
    uint32_t onPassKeyRequest()     // pairing password PIN
    {
      Serial.printf("Pairing password: %d \r\n", PIN); 
      return PIN;                   // Authentication / Security
    }
    void onPassKeyNotify(uint32_t pass_key)  // key
    {
    }
   
    bool onConfirmPIN(uint32_t pass_key)
    {
      return true;
    }
 
    bool onSecurityRequest() {       
      Serial.printf("Sicherheitsanfrage\r\n");
      return true;
    }
    void onAuthenticationComplete(esp_ble_auth_cmpl_t auth_cmpl) 
    {
      if (auth_cmpl.success)        // Bluetooth Authentication erfolgreich
      {
      }
      else
      {
       Serial.println("Authentication durchgefallen. Falsche PIN ?");
       Verbinde = false;
      }
    }
};
 
// BLE Callbacks
 
static void notifyCallback        // Es ist was per Bluetooth vom HM-10 empfangen wurde
(
  BLERemoteCharacteristic* pBLERemoteCharacteristic,
  uint8_t* pData,
  size_t length,
  bool isNotify)
{
  String EingangDaten = "";
  for (int i = 0; i < length; i++)EingangDaten += char(*pData++); // Byte als Zeichen an String anhängen. Zum nächster Speicherstelle wechseln
  Serial.println(EingangDaten);
}
 
// Verbinde mit BLE Server. Peripherie Gerät HM10 stelellt ein Server bereit. (ist schon verwirrend)
 
bool connectToServer(BLEAddress pAddress)
{
  Serial.print("Verbinde mit ");
  Serial.println(pAddress.toString().c_str());
 
  BLEDevice::setEncryptionLevel(ESP_BLE_SEC_ENCRYPT);
  BLEDevice::setSecurityCallbacks(new MySecurity());
 
  BLESecurity *pSecurity = new BLESecurity();
  pSecurity->setAuthenticationMode(ESP_LE_AUTH_BOND); //
  pSecurity->setCapability(ESP_IO_CAP_KBDISP);
  pSecurity->setRespEncryptionKey(ESP_BLE_ENC_KEY_MASK | ESP_BLE_ID_KEY_MASK);
 
  pClient = BLEDevice::createClient();
  // Verbinde zum HM10 mit Password PIN.
  if ( pClient->connect(pAddress) ) Serial.println("Verbunden");
  
// Beziehen eines Verweises auf benötigte charakteristik
  BLERemoteService* pRemoteService = pClient->getService(serviceUUID);
  if (pRemoteService == nullptr)
  {
    Serial.print("Gefunden falsche UUID: ");
    Serial.println(serviceUUID.toString().c_str());
    return false;
  }
 
  // Beziehen eines Verweises auf benötigte charakteristik
  pRemoteCharacteristic = pRemoteService->getCharacteristic(charUUID);
  if (pRemoteCharacteristic == nullptr) {
    Serial.print("Gefunden falsche Characteristic UUID: ");
    Serial.println(charUUID.toString().c_str());
    return false;
  }
 
  // hier wird das Unterprogramm angebunden die Daten vom Bluetooth Empfang bearbeitet
  pRemoteCharacteristic->registerForNotify(notifyCallback);
  return true;
}


void setup() {
   BLEDevice::init("");
   Serial.begin(19200);
   //Serial2.begin(19200);
}

void loop() {
   if (Verbinde == true) // Paaren
  {
    pServerAddress = new BLEAddress(HM_MAC);
    if (connectToServer(*pServerAddress))
    {
      Verbunden = true;
      Verbinde = false;
    }
  }
//
//   while (Serial2.available()) {
//    Serial.write(Serial2.read());
//   }

   //Serial.print("X: "); Serial.println(analogRead(A2));
   //Serial.print("Y: "); Serial.println(analogRead(A3));
   int32_t vertical   = map(analogRead(A2)+200, 0, 4096,  0,  180);
   int32_t horizontal = map(analogRead(A3)+220, 0, 4096,  -50, 50);  //left to right
   int32_t left       = vertical + horizontal;
   int32_t right      = vertical - horizontal;

   left  = max(left, 0);
   left  = min(left, 180);
   right = max(right, 0);
   right = min(right, 180);

   if (abs(left-90) < 5){
    left = 90;
   }

   if (abs(right-90) < 5){
    right = 90;
   }

   String a = "/motorControl?";
   a+=left;
   a+="=";
   a+=right;   
   
   Serial.println(a);
   //Serial2.println(a);
    
  if (Verbunden)
  {
    pRemoteCharacteristic->writeValue(a.c_str(), a.length());
  }
   delay(300);
}     
