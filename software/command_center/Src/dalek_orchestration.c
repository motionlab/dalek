#include "main.h"
#include "stdbool.h"
#include "cmsis_os.h"
#include "dalek_i2c.h"
#include "queue.h"
#include "dalek_orchestra.h"
#include "dalek_orchestration.h"
#include "timers.h"


void DalekOrchestrationTask(void *argument){

  while(true){
    osDelay(1000);
    DalekOrchestraHangarOpen(false);
    DalekOrchestraHangarLights(0);
    osDelay(300*1000);
    DalekOrchestraHangarOpen(true);
    DalekOrchestraHangarLights(4095);
    osDelay(300*1000);
  }
}
