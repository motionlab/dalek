#ifndef DALEK_ORCHESTRA_H
#define DALEK_ORCHESTRA_H

#define DALEK_ORCHESTRA_STEPPER_HIGHTIME    1
#define DALEK_ORCHESTRA_STEPPER_INTERVAL   10
#define DALEK_ORCHESTRA_LIGHT_INTERVAL     50
#define DALEK_ORCHESTRA_BLINK_INTERVAL    500
#define DALEK_ORCHESTRA_FOGGER_INTERVAL  1000
#define DALEK_ORCHESTRA_HATCH_OPEN          0
#define DALEK_ORCHESTRA_HATCH_CLOSED      322
#define DALEK_ORCHESTRA_HATCH_OUT_TICKS   981
#define DALEK_ORCHESTRA_AIR_PUMP_ON      4096
#define DALEK_ORCHESTRA_AIR_PUMP_OFF        0
#define DALEK_ORCHESTRA_FOGGER_ON        4096
#define DALEK_ORCHESTRA_FOGGER_OFF          0

typedef enum{
    inbound=0,
    outbound=1
} t_HangarDirection;

void DalekOrchestraEyeBlink    (uint32_t amount);
void DalekOrchestraHangarOpen  (bool     state );
void DalekOrchestraHangarLights(uint32_t value ); //Max 4095
void DalekOrchestraFogger      (bool     state );

#endif