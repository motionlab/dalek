#include "main.h"
#include "stdbool.h"
#include "cmsis_os.h"
#include "dalek_i2c.h"
#include "queue.h"
#include "dalek_orchestra.h"
#include "timers.h"

xTimerHandle mDalekOrchestraHangarStepperTimerHandle  = NULL;
xTimerHandle mDalekOrchestraHangarMovementTimerHandle = NULL;
xTimerHandle mDalekOrchestraHangarLightsTimerHandle   = NULL;
xTimerHandle mDalekOrchestraEyeTimerHandle            = NULL;
xTimerHandle mDalekOrchestraFoggerTimerHandle         = NULL;
bool         mDalekOrchestraEyeLEDIsON                = false;
uint64_t     mDalekOrchestraHangarStepCounter         = 0;
uint16_t     mDalekOrchestraHangarLightValue          = 0;

///////////////////////////////////////////////////////////////////////////////////////
// private functions
///////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Hangar //////////////////////////////////////////////////////
void DalekOrchestraHangarStepStop(TimerHandle_t xTimer){
    HAL_GPIO_WritePin(Hangar_Step_GPIO_Port, Hangar_Step_Pin, 0);
    xTimerDelete(xTimer, 0);
}

void DalekOrchestraHangarEnable(bool value){
    HAL_GPIO_WritePin(Hangar_Stepper_Enable_GPIO_Port, Hangar_Stepper_Enable_Pin, !value); // input inverted
}

void DalekOrchestraHangarStep(t_HangarDirection direction){
    HAL_GPIO_WritePin(Hangar_Direction_GPIO_Port, Hangar_Direction_Pin, direction);
    HAL_GPIO_WritePin(Hangar_Step_GPIO_Port, Hangar_Step_Pin, 1);

    if(direction==outbound){
        mDalekOrchestraHangarStepCounter++;
    }else{
        if(mDalekOrchestraHangarStepCounter > 0){
            mDalekOrchestraHangarStepCounter--;
        }
    }

    mDalekOrchestraHangarStepperTimerHandle = xTimerCreate("HangarStepper", pdMS_TO_TICKS(DALEK_ORCHESTRA_STEPPER_HIGHTIME), pdFALSE, (void *)23, DalekOrchestraHangarStepStop);
    if (mDalekOrchestraHangarStepperTimerHandle==NULL) {
        //error
    }else{
        if (xTimerStart( mDalekOrchestraHangarStepperTimerHandle, 0) != pdPASS){
            //error
        }
    }
}

void DalekOrchestraHangarMovement(TimerHandle_t xTimer){

    if((bool)pvTimerGetTimerID(xTimer)){
        //Open
        if(mDalekOrchestraHangarStepCounter < DALEK_ORCHESTRA_HATCH_OUT_TICKS){
            DalekOrchestraHangarStep(outbound);
        }else{
            xTimerStop(xTimer, 0);
            xTimerDelete(xTimer, 0);
            DalekOrchestraHangarEnable(false);
        }
    }else{
        //Close High=endstop reached
        if(HAL_GPIO_ReadPin(Hangar_End_Stop_GPIO_Port, Hangar_End_Stop_Pin)){
            xTimerStop(xTimer, 0);
            xTimerDelete(xTimer, 0);
            DalekOrchestraHangarEnable(false);
            mDalekOrchestraHangarStepCounter=0;
            DalekI2cQueueMessage_t m;
            m.device  = DALEK_I2C_DEVICE_HANGAR_HATCH;
            m.address = DALEK_I2C_ADDRESS_HANGAR_HATCH;
            m.value   = DALEK_ORCHESTRA_HATCH_CLOSED;
            xQueueSend(dalekI2cQueueHandle, &m, 0);
        }else{
            DalekOrchestraHangarStep(inbound);
        }
    }
}

void DalekOrchestraHangarLightsChanger(TimerHandle_t xTimer){

    uint32_t value = (uint32_t)pvTimerGetTimerID(xTimer);

    if(mDalekOrchestraHangarLightValue > value){
        mDalekOrchestraHangarLightValue--;
    }else if(mDalekOrchestraHangarLightValue < value){
        mDalekOrchestraHangarLightValue++;
    }else{
        xTimerStop(xTimer, 0);
        xTimerDelete(xTimer, 0);
    }

    DalekI2cQueueMessage_t m;
    m.device  = DALEK_I2C_DEVICE_DRONE_HANGAR_LIGHT;
    m.address = DALEK_I2C_ADDRESS_DRONE_HANGAR_LIGHT;
    m.value   = mDalekOrchestraHangarLightValue;
    xQueueSend(dalekI2cQueueHandle, &m, 0);
}

///////////////////////// Dalek //////////////////////////////////////////////////////

void DalekOrchestraEyeBlinker(TimerHandle_t xTimer){
  uint32_t blinkCounter = (uint32_t)pvTimerGetTimerID(xTimer);
  DalekI2cQueueMessage_t m;
  m.device  = DALEK_I2C_DEVICE_EYE;
  m.address = DALEK_I2C_ADDRESS_EYE;

  if(blinkCounter == 0){
    xTimerStop(xTimer, 0);
    xTimerDelete(xTimer, 0);
    m.value = 0;
    mDalekOrchestraEyeLEDIsON = false;
  }else{
    if (mDalekOrchestraEyeLEDIsON){
      m.value = 1024;
      mDalekOrchestraEyeLEDIsON = true;
    }else{
      blinkCounter--;
      m.value = 0;
      mDalekOrchestraEyeLEDIsON = false;
    }
    xQueueSend(dalekI2cQueueHandle, &m, 0);
    vTimerSetTimerID(xTimer, (void *)blinkCounter);
  }
}

///////////////////////// Fogger //////////////////////////////////////////////////////
void DalekOrchestraFoggerToggler(TimerHandle_t xTimer){
  uint32_t currentfoggerstate = (uint32_t)pvTimerGetTimerID(xTimer);
  DalekI2cQueueMessage_t m;
  m.device  = DALEK_I2C_DEVICE_FOGGER;
  m.address = DALEK_I2C_ADDRESS_FOGGER;

  if(currentfoggerstate){
    m.value = DALEK_ORCHESTRA_FOGGER_OFF;
    vTimerSetTimerID(xTimer, (void *)0);
  }else{
    m.value = DALEK_ORCHESTRA_FOGGER_ON;
    vTimerSetTimerID(xTimer, (void *)1);
  }
  xQueueSend(dalekI2cQueueHandle, &m, 0);
}

///////////////////////////////////////////////////////////////////////////////////////
// Public Functions
///////////////////////////////////////////////////////////////////////////////////////
void DalekOrchestraEyeBlink(uint32_t amount){
  mDalekOrchestraEyeTimerHandle = xTimerCreate("eyetimer", pdMS_TO_TICKS(DALEK_ORCHESTRA_BLINK_INTERVAL), pdTRUE, (void *)amount, DalekOrchestraEyeBlinker);
  if (mDalekOrchestraEyeTimerHandle==NULL) {
    //error
  }else{
    if (xTimerStart( mDalekOrchestraEyeTimerHandle, 0) != pdPASS){
      //error
    }
  }
}

void DalekOrchestraHangarOpen(bool state){

  if(state){
    DalekI2cQueueMessage_t m;
    m.device  = DALEK_I2C_DEVICE_HANGAR_HATCH;
    m.address = DALEK_I2C_ADDRESS_HANGAR_HATCH;
    m.value   = DALEK_ORCHESTRA_HATCH_OPEN;
    xQueueSend(dalekI2cQueueHandle, &m, 0);
  }

  DalekOrchestraHangarEnable(true);
  mDalekOrchestraHangarMovementTimerHandle = xTimerCreate("hangarmovertimer", pdMS_TO_TICKS(DALEK_ORCHESTRA_STEPPER_INTERVAL), pdTRUE, (void *)state, DalekOrchestraHangarMovement);
  if (mDalekOrchestraHangarMovementTimerHandle==NULL) {
    //error
  }else{
    if (xTimerStart( mDalekOrchestraHangarMovementTimerHandle, 0) != pdPASS){
      //error
    }
  }
}

void DalekOrchestraHangarLights(uint32_t value){

  if(value > 4095){
      value = 4095;
  }

  if(value != mDalekOrchestraHangarLightValue){
    mDalekOrchestraHangarLightsTimerHandle = xTimerCreate("hangarlighttimer", pdMS_TO_TICKS(DALEK_ORCHESTRA_LIGHT_INTERVAL), pdTRUE, (void *)value, DalekOrchestraHangarLightsChanger);
    if (mDalekOrchestraHangarLightsTimerHandle==NULL) {
        //error
    }else{
        if (xTimerStart( mDalekOrchestraHangarLightsTimerHandle, 0) != pdPASS){
        //error
        }
    }
  }
}

void DalekOrchestraFogger(bool state){

  if (state){
    DalekI2cQueueMessage_t m;
    m.device  = DALEK_I2C_DEVICE_AIR_PUMP_FOGGER;
    m.address = DALEK_I2C_ADDRESS_AIR_PUMP_FOGGER;
    m.value   = DALEK_ORCHESTRA_AIR_PUMP_ON;
    xQueueSend(dalekI2cQueueHandle, &m, 0);
    m.device  = DALEK_I2C_DEVICE_FOGGER;
    m.address = DALEK_I2C_ADDRESS_FOGGER;
    m.value   = DALEK_ORCHESTRA_FOGGER_ON;
    xQueueSend(dalekI2cQueueHandle, &m, 0);

    mDalekOrchestraFoggerTimerHandle = xTimerCreate("hangarfogger", pdMS_TO_TICKS(DALEK_ORCHESTRA_FOGGER_INTERVAL), pdTRUE, (void *)1, DalekOrchestraFoggerToggler);
    if (mDalekOrchestraFoggerTimerHandle==NULL) {
        //error
    }else{
        if (xTimerStart( mDalekOrchestraFoggerTimerHandle, 0) != pdPASS){
        //error
        }
    }
  }else{
    xTimerStop(mDalekOrchestraFoggerTimerHandle, 0);
    xTimerDelete(mDalekOrchestraFoggerTimerHandle, 0);
    DalekI2cQueueMessage_t m;
    m.device  = DALEK_I2C_DEVICE_AIR_PUMP_FOGGER;
    m.address = DALEK_I2C_ADDRESS_AIR_PUMP_FOGGER;
    m.value   = DALEK_ORCHESTRA_AIR_PUMP_OFF;
    xQueueSend(dalekI2cQueueHandle, &m, 0);
    m.device  = DALEK_I2C_DEVICE_FOGGER;
    m.address = DALEK_I2C_ADDRESS_FOGGER;
    m.value   = DALEK_ORCHESTRA_FOGGER_OFF;
    xQueueSend(dalekI2cQueueHandle, &m, 0);
  }
}
