#include <Arduino.h>
#include "globals.h"

void iFunction() {
  ping =  micros();
  Serial.print("ping");
  waitingForInterrupt = false;
}

void setup() {
    pinMode(     PIN_SAFETY,    OUTPUT);
    digitalWrite(PIN_SAFETY,    LOW   );
    pinMode(     PIN_INDICATOR, OUTPUT);
    digitalWrite(PIN_INDICATOR, LOW   );
    pinMode(     PIN_TRIGGER,   OUTPUT);
    Serial.begin(9600);
    Serial.print("setup done");
}

void loop() {

  switch (state) {
    case trigger:
      Serial.print("triggering");
      digitalWrite(PIN_TRIGGER, LOW);
      delayMicroseconds(5);
      digitalWrite(PIN_TRIGGER, HIGH);
      delayMicroseconds(10);
      digitalWrite(PIN_TRIGGER, LOW);
      triggerTime = micros();
      waitingForInterrupt = true;
      attachInterrupt(digitalPinToInterrupt(3), iFunction, FALLING);
      state = wait;
    break;

    case wait:
      //Serial.print("waiting");
      if (!waitingForInterrupt) {
        detachInterrupt(digitalPinToInterrupt(PIN_ECHO));
        state = calculate;
      }
    break;

    case calculate:
      if (ping - triggerTime <= SAFETY_DISTANCE) {
        Serial.println("Alarm!");
        digitalWrite(PIN_SAFETY,    LOW);
        digitalWrite(PIN_INDICATOR, LOW);
      }
      else {
        digitalWrite(PIN_SAFETY,    HIGH);
        digitalWrite(PIN_INDICATOR, HIGH);
      }
      state = trigger;
      break;
  }
}
