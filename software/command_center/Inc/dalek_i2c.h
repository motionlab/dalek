
#ifndef DALEK_I2C_H
#define DALEK_I2C_H

<<<<<<< HEAD
#define I2C_MODULE_1 0x40

#define LED0_ON_L       0x06 
#define LED0_ON_H       0x07 
#define LED0_OFF_L      0x08 
#define LED0_OFF_H      0x09 

#define ALL_LED_ON_L    0xFA
#define ALL_LED_ON_H    0xFB
#define ALL_LED_OFF_L   0xFC
#define ALL_LED_OFF_H   0xFD
#define PRE_SCALE       0xFE


#define PCA9685_SUBADR1 0x2 /**< i2c bus address 1 */
#define PCA9685_SUBADR2 0x3 /**< i2c bus address 2 */
#define PCA9685_SUBADR3 0x4 /**< i2c bus address 3 */

#define MODE1_REG       0x00
#define MODE2_REG       0x01 


||||||| merged common ancestors
=======
//########################################################
#include "queue.h"

//########################################################
#define DALEK_I2C_DEVICE_LEFT_STEARING_JOYSTICK   0x00;
#define DALEK_I2C_ADDRESS_LEFT_STEARING_JOYSTICK  0x00;
#define DALEK_I2C_DEVICE_RIGHT_STEARING_JOYSTICK  0x00;
#define DALEK_I2C_ADDRESS_RIGHT_STEARING_JOYSTICK 0x01;
#define DALEK_I2C_DEVICE_HANGAR_HATCH             0x00;
#define DALEK_I2C_ADDRESS_HANGAR_HATCH            0x02;
#define DALEK_I2C_DEVICE_ROBOT_ARM_GROUND         0x00;
#define DALEK_I2C_ADDRESS_ROBOT_ARM_GROUND        0x03;
#define DALEK_I2C_DEVICE_ROBOT_ARM_SHOULDER       0x00;
#define DALEK_I2C_ADDRESS_ROBOT_ARM_SHOULDER      0x04;
#define DALEK_I2C_DEVICE_ROBOT_ARM_HAND           0x00;
#define DALEK_I2C_ADDRESS_ROBOT_ARM_HAND          0x05;
#define DALEK_I2C_DEVICE_DISCO_BALL               0x00;
#define DALEK_I2C_ADDRESS_DISCO_BALL              0x06;
#define DALEK_I2C_DEVICE_DALEK_DANCE              0x00;
#define DALEK_I2C_ADDRESS_DALEK_DANCE             0x07;
#define DALEK_I2C_DEVICE_FIRST_GAUGE              0x00;
#define DALEK_I2C_ADDRESS_FIRST_GAUGE             0x08;
#define DALEK_I2C_DEVICE_SECOND_GAUGE             0x00;
#define DALEK_I2C_ADDRESS_SECOND_GAUGE            0x09;
#define DALEK_I2C_DEVICE_THIRD_GAUGE              0x00;
#define DALEK_I2C_ADDRESS_THIRD_GAUGE             0x0A;
#define DALEK_I2C_DEVICE_FIRST_GAUGE_LIGHT        0x00;
#define DALEK_I2C_ADDRESS_FIRST_GAUGE_LIGHT       0x0B;
#define DALEK_I2C_DEVICE_SECOND_GAUGE_LIGHT       0x00;
#define DALEK_I2C_ADDRESS_SECOND_GAUGE_LIGHT      0x0C;
#define DALEK_I2C_DEVICE_THIRD_GAUGE_LIGHT        0x00;
#define DALEK_I2C_ADDRESS_THIRD_GAUGE_LIGHT       0x0D;
#define DALEK_I2C_DEVICE_AIR_PUMP_FOGGER          0x00;
#define DALEK_I2C_ADDRESS_AIR_PUMP_FOGGER         0x0E;
#define DALEK_I2C_DEVICE_DRONE_HANGAR_LIGHT       0x00;
#define DALEK_I2C_ADDRESS_DRONE_HANGAR_LIGHT      0x0F;

#define DALEK_I2C_DEVICE_SEAT_LIGHT_R             0x01;
#define DALEK_I2C_ADDRESS_SEAT_LIGHT_R            0x00;
#define DALEK_I2C_DEVICE_SEAT_LIGHT_G             0x01;
#define DALEK_I2C_ADDRESS_SEAT_LIGHT_G            0x01;
#define DALEK_I2C_DEVICE_SEAT_LIGHT_B             0x01;
#define DALEK_I2C_ADDRESS_SEAT_LIGHT_B            0x02;
#define DALEK_I2C_DEVICE_MAIN_LIGHT_R             0x01;
#define DALEK_I2C_ADDRESS_MAIN_LIGHT_R            0x03;
#define DALEK_I2C_DEVICE_MAIN_LIGHT_G             0x01;
#define DALEK_I2C_ADDRESS_MAIN_LIGHT_G            0x04;
#define DALEK_I2C_DEVICE_MAIN_LIGHT_B             0x01;
#define DALEK_I2C_ADDRESS_MAIN_LIGHT_B            0x05;
#define DALEK_I2C_DEVICE_DISCO_BALL_R             0x01;
#define DALEK_I2C_ADDRESS_DISCO_BALL_R            0x06;
#define DALEK_I2C_DEVICE_DISCO_BALL_G             0x01;
#define DALEK_I2C_ADDRESS_DISCO_BALL_G            0x07;
#define DALEK_I2C_DEVICE_DISCO_BALL_B             0x01;
#define DALEK_I2C_ADDRESS_DISCO_BALL_B            0x08;
#define DALEK_I2C_DEVICE_FAN                      0x01;
#define DALEK_I2C_ADDRESS_FAN                     0x09;
#define DALEK_I2C_DEVICE_FOGGER                   0x01;
#define DALEK_I2C_ADDRESS_FOGGER                  0x0A;
#define DALEK_I2C_DEVICE_FIRST_LASER_DIODE        0x01;
#define DALEK_I2C_ADDRESS_FIRST_LASER_DIODE       0x0B;
#define DALEK_I2C_DEVICE_SECOND_LASER_DIODE       0x01;
#define DALEK_I2C_ADDRESS_SECOND_LASER_DIODE      0x0C;
#define DALEK_I2C_DEVICE_THIRD_LASER_DIODE        0x01;
#define DALEK_I2C_ADDRESS_THIRD_LASER_DIODE       0x0D;
#define DALEK_I2C_DEVICE_FORTH_LASER_DIODE        0x01;
#define DALEK_I2C_ADDRESS_FORTH_LASER_DIODE_      0x0E;
#define DALEK_I2C_DEVICE_FIFTH_LASER_DIODE        0x01;
#define DALEK_I2C_ADDRESS_FIFTH_LASER_DIODE       0x0F;

#define DALEK_I2C_DEVICE_FIRST_COMPUTER_LEFT      0x02;
#define DALEK_I2C_ADDRESS_FIRST_COMPUTER_LEFT     0x00;
#define DALEK_I2C_DEVICE_FIRST_COMPUTER_RIGHT     0x02;
#define DALEK_I2C_ADDRESS_FIRST_COMPUTER_RIGHT    0x01;
#define DALEK_I2C_DEVICE_SECOND_COMPUTER_LEFT     0x02;
#define DALEK_I2C_ADDRESS_SECOND_COMPUTER_LEFT    0x02;
#define DALEK_I2C_DEVICE_SECOND_COMPUTER_RIGHT    0x02;
#define DALEK_I2C_ADDRESS_SECOND_COMPUTER_RIGHT   0x03;
#define DALEK_I2C_DEVICE_THIRD_COMPUTER_LEFT      0x02;
#define DALEK_I2C_ADDRESS_THIRD_COMPUTER_LEFT     0x04;
#define DALEK_I2C_DEVICE_THIRD_COMPUTER_RIGHT     0x02;
#define DALEK_I2C_ADDRESS_THIRD_COMPUTER_RIGHT    0x05;
#define DALEK_I2C_DEVICE_WASHING_MACHINE          0x02;
#define DALEK_I2C_ADDRESS_WASHING_MACHINE         0x06;
#define DALEK_I2C_DEVICE_LEFT_USB_DISCO           0x02;
#define DALEK_I2C_ADDRESS_LEFT_USB_DISCO          0x07;
#define DALEK_I2C_DEVICE_RIGHT_USB_DISCO          0x02;
#define DALEK_I2C_ADDRESS_RIGHT_USB_DISCO         0x08;
#define DALEK_I2C_DEVICE_FLAG                     0x02;
#define DALEK_I2C_ADDRESS_FLAG                    0x09;
#define DALEK_I2C_DEVICE_EYE                      0x02;
#define DALEK_I2C_ADDRESS_EYE                     0x0A;
#define DALEK_I2C_DEVICE_R1                       0x02;
#define DALEK_I2C_ADDRESS_R1                      0x0B;
#define DALEK_I2C_DEVICE_R2                       0x02;
#define DALEK_I2C_ADDRESS_R2                      0x0C;
#define DALEK_I2C_DEVICE_R3                       0x02;
#define DALEK_I2C_ADDRESS_R3                      0x0D;
#define DALEK_I2C_DEVICE_R4                       0x02;
#define DALEK_I2C_ADDRESS_R4                      0x0E;
#define DALEK_I2C_DEVICE_R5                       0x02;
#define DALEK_I2C_ADDRESS_R6                      0x0F;

//########################################################
typedef struct{
    uint8_t  device;
    uint8_t  address;
    uint16_t value;
} DalekI2cQueueMessage_t;

//########################################################
void DalekI2cTask(void *argument);

//########################################################
QueueHandle_t dalekI2cQueueHandle;

>>>>>>> da3354db685389c4ba92a87478897585d0f9851c
#endif