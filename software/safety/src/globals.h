#ifndef GLOBALS_H
#define GLOBALS_H

#define PIN_SAFETY        2
#define PIN_ECHO          3
#define PIN_INDICATOR    A4
#define PIN_TRIGGER      A5

#define SAFETY_DISTANCE 3000

enum States {
  trigger,
  wait,
  calculate
};

volatile uint32_t triggerTime, ping = 0;
volatile bool waitingForInterrupt = false;
States state = trigger;

#endif
