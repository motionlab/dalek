//
//  main.m
//  dalekremote
//
//  Created by Daniel Kalwitzki on 11.03.19.
//  Copyright © 2019 Daniel Kalwitzki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
