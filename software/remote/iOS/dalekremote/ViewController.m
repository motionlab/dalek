//
//  ViewController.m
//  dalekremote
//
//  Created by Daniel Kalwitzki on 11.03.19.
//  Copyright © 2019 Daniel Kalwitzki. All rights reserved.
//

#import "ViewController.h"
#import "BLEHandler.h"
#import "JoyStickView.h"
#import <AVKit/AVKit.h>

@interface ViewController ()

@end

@implementation ViewController


Boolean dance = NO;
Boolean giggle = NO;
Boolean light1 = 0;
Boolean light2 = 0;
Boolean light3 = 0;
int helper = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [BLEHandler sharedInstance];
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver: self
                           selector: @selector (onStickChanged:)
                               name: @"StickChanged"
                             object: nil];
    [NSTimer scheduledTimerWithTimeInterval:0.1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        if (nil != self.sendval) {
            [[BLEHandler sharedInstance] writeValue:[NSString stringWithFormat:@"%@",self->_sendval]];
           
//            [[BLEHandler sharedInstance] writeValue:[NSString stringWithFormat:@"Light=%d/",helper]];
        //NSLog(@"%@", self.sendval);
        }
    }];
    [NSTimer scheduledTimerWithTimeInterval:0.7 repeats:YES block:^(NSTimer * _Nonnull timer) {
//        int helper = 0;
//        if (light1) {
//            helper = helper +1;
//        }
//        if (light2) {
//            helper = helper +2;
//        }
//        if (light3) {
//            helper = helper +4;
//        }
        if (dance) {
            if ([self->_sendval isEqualToString:@"60=120"]) {
                self->_sendval = [NSString stringWithFormat:@"120=60"];
            } else {
                self->_sendval = [NSString stringWithFormat:@"60=120"];
            }
        }
    }];
    [NSTimer scheduledTimerWithTimeInterval:0.2 repeats:YES block:^(NSTimer * _Nonnull timer) {
        
        if (giggle) {
            if ([self->_sendval isEqualToString:@"60=120"]) {
                self->_sendval = [NSString stringWithFormat:@"120=60"];
            } else {
                self->_sendval = [NSString stringWithFormat:@"60=120"];
            }
        }
    }];
    
}

- (IBAction)light1:(id)sender {
    if (light1) {
        light1 = NO;
    } else {
        light1 = YES;
    }
}

- (IBAction)light2:(id)sender {
    if (light2) {
        light2 = NO;
    } else {
        light2 = YES;
    }
}

- (IBAction)light3:(id)sender {
    if (light3) {
        light3 = NO;
    } else {
        light3 = YES;
    }
}

-(void) playFile:(NSString*)file withType:(NSString*)type {
    NSString *path = [[NSBundle mainBundle] pathForResource:file ofType:type];
    NSError *error;
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    NSURL *url = [NSURL fileURLWithPath:path];
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_player setDelegate:self];
    [_player prepareToPlay];
    [_player play];
}

-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error {
    NSLog(error.userInfo);
}



- (IBAction)say1:(id)sender {
    [self playFile:@"exterminate" withType:@"wav"];
}

- (IBAction)say2:(id)sender {
     [self playFile:@"explain" withType:@"wav"];
}

- (IBAction)say3:(id)sender {
     [self playFile:@"dont-take-oders" withType:@"wav"];
}

- (IBAction)say4:(id)sender {
      [self playFile:@"donotinterrupt" withType:@"mp3"];
}


- (IBAction)endButton:(id)sender {
     dance = NO;
    giggle = NO;
     _sendval = [NSString stringWithFormat:@"90=90"];
}

- (IBAction)dance:(id)sender {
    dance = YES;
}

- (IBAction)shake:(id)sender {
    giggle = YES;
}

- (IBAction)turn:(id)sender {
     _sendval = [NSString stringWithFormat:@"120=60"];
}

- (void)onStickChanged:(NSNotification*)notification
{
    NSDictionary *dict = [notification userInfo];
    NSValue *vdir = [dict valueForKey:@"dir"];
    CGPoint dir = [vdir CGPointValue];

    _sendval = [NSString stringWithFormat:@"%.f=%.f",dir.y,dir.x];
}

- (IBAction)moveForward:(id)sender {
}


@end
